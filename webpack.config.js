const CopyWebpackPlugin = require('copy-webpack-plugin');
const htmlwebpackplugin = require('html-webpack-plugin')
const webpack = require('webpack');
require('file-loader');
require('html-loader');
require('style-loader');
require('svg-inline-loader');
require('svgo-loader');
const path = require('path');

module.exports = {
    mode: 'development',
    entry: './src/index.js',
    output: {
        filename: 'main.bundle.js',
        path: path.resolve(__dirname, 'dist')
    },
    module: {
        rules: [
            {
                test: /\.svg$/,
                use: [
                  {
                    loader: 'file-loader'
                  },
                  {
                    loader: 'svgo-loader',
                  }
                ]
            },
            {
                test: /\.css$/,
                exclude: /node_modules/,
                use: [{ loader: 'style-loader' }]
            },
            {
                test: /\.(glb|gltf)$/,
                use: [{ loader: 'file-loader', options: { outputPath: 'assets/models/' }}]
            },
            {
                test: /\.html$/i,
                loader: "html-loader",
                exclude: /node_modules/,
            },
            {
                test: /\.(png|jpe?g|gif)$/i,
                exclude: /node_modules/,
                use: [{ loader: 'file-loader' }],
            },
            {
                test: /\.js$/,
                exclude: /node_modules/
            },
        ]
    },
    devServer: {
        contentBase: './dist',
        overlay: true,
        hot: true
    },
    plugins: [
        new CopyWebpackPlugin(['index.html', 'index.css']),
        new webpack.HotModuleReplacementPlugin(),

    ]
};
